import React, { Component, Fragment } from "react";
import AddUser from "../component/addUser";
import * as Connector from "../module/connector";
import {
  Button,
  Confirm,
  Dimmer,
  Grid,
  Header,
  Message,
  Segment,
  Table,
  Icon,
  Loader,
  Divider,
} from "semantic-ui-react";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clearControls: false,
      deleteLoading: false,
      notificationMessage: "",
      notificationType: "",
      transactionType: "Add User",
      selectedUserId: 0,
      showDeleteConfirm: false,
      userInfo: [],
      userList: [],
    };
  }

  componentDidMount() {
    this.getUserList();
  }

  deleteUser = () => {
    const { selectedUserId } = this.state;
    this.setState({
      clearControls: true,
      deleteLoading: true,
      showDeleteConfirm: false,
      userInfo: [],
    });
    Connector.del(`/users/deleteuser/${selectedUserId}`)
      .then((response) => {
        this.transactionCompleted(response.success, response.msg, "delete");
      })
      .catch((err) => {
        this.transactionCompleted(
          "error",
          "Some error occured while deleting records",
          "delete"
        );
        this.setState({ deleteLoading: false });
        return;
      });
  };

  getUserList = () => {
    Connector.get("/users/userlist")
      .then((response) => {
        if (response && response.success && response.success === false) {
          this.transactionCompleted(response.success, response.msg, "getList");
          return;
        }
        const userList = response;
        this.setState({
          deleteLoading: false,
          userList: userList,
          selectedUserId: 0,
          showDeleteConfirm: false,
          transactionType: "Add User",
          userInfo: [],
        });
      })
      .catch((err) => {
        this.transactionCompleted(
          "error",
          "Some error occured while fetching records",
          "getList"
        );
        return;
      });
  };

  showUserInfo = (userId) => {
    const { userList } = this.state;
    let userInfo = [];
    userInfo = userList.filter((el) => el._id === userId)[0];
    this.setState({
      clearControls: false,
      userInfo: userInfo,
      selectedUserId: userId,
      transactionType: "Modify User",
    });
  };

  transactionCompleted = (success, message, transType) => {
    if (success === true) {
      NotificationManager.success(message);
      if (transType !== "getList") this.getUserList();
    } else {
      NotificationManager.error(message);
      this.setState({ loading: false });
    }
  };

  render() {
    const {
      clearControls,
      deleteLoading,
      displayErrorMessage,
      errorMessage,
      loading,
      notificationMessage,
      notificationType,
      selectedUserId,
      showDeleteConfirm,
      transactionType,
      userInfo,
      userList,
    } = this.state;

    return (
      <Fragment>
        <Grid columns="equal" style={{ marginTop: "1em" }}>
          <Grid.Column>
            <Segment style={{ marginLeft: "1em", height: "330px" }}>
              <Header
                as="h3"
                block
                style={{ backgroundColor: "#00b5ad", color: "white" }}
              >
                User List
              </Header>

              <Dimmer active={deleteLoading} inverted>
                <Loader>Deleting...</Loader>
              </Dimmer>
              <Table
                celled
                className="tbody"
                fixed
                selectable
                size="small"
                striped
              >
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>User Name</Table.HeaderCell>
                    <Table.HeaderCell>Email</Table.HeaderCell>
                    <Table.HeaderCell>Delete</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  {userList.map((user) => (
                    <Table.Row
                      onClick={() => {
                        this.showUserInfo(user._id);
                      }}
                    >
                      <Table.Cell>{user.username}</Table.Cell>
                      <Table.Cell>{user.email}</Table.Cell>
                      <Table.Cell textAlign="center">
                        <a href="#">
                          <Icon
                            name="trash"
                            onClick={() => {
                              this.setState({
                                selectedUserId: user._id,
                                showDeleteConfirm: true,
                              });
                            }}
                          />
                        </a>
                      </Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            </Segment>
          </Grid.Column>
          <Grid.Column width={8}>
            <Segment>
              <AddUser
                clearControls={clearControls}
                selectedUserId={selectedUserId}
                transactionType={transactionType}
                transactionCompleted={this.transactionCompleted}
                userInfo={userInfo}
              />
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment style={{ marginRight: "1em", height: "330px" }}>
              <Header
                as="h3"
                block
                style={{ backgroundColor: "#00b5ad", color: "white" }}
              >
                User Info
              </Header>

              <p style={{ textAlign: "left" }}>
                <b style={{ marginRight: "5px" }}>Name: </b> {userInfo.username}
              </p>
              <Divider />
              <p style={{ textAlign: "left" }}>
                <b style={{ marginRight: "5px" }}>Age:</b> {userInfo.age}
              </p>
              <Divider />
              <p style={{ textAlign: "left" }}>
                <b style={{ marginRight: "5px" }}>Gender:</b> {userInfo.gender}
              </p>
              <Divider />
              <p style={{ textAlign: "left" }}>
                <b style={{ marginRight: "5px" }}>Locatlion:</b>{" "}
                {userInfo.location}
              </p>
              <Divider />
            </Segment>
          </Grid.Column>
        </Grid>
        <Confirm
          header="Are you sure you want to delete the selected record"
          onCancel={() => {
            this.setState({ showDeleteConfirm: false });
          }}
          onConfirm={() => {
            this.deleteUser();
          }}
          open={showDeleteConfirm}
        />
        <NotificationContainer />
      </Fragment>
    );
  }
}
export default Index;
