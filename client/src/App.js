import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Index from "./view/index";
import "./css/style.css";
import "semantic-ui-css/semantic.min.css";
import "react-notifications/lib/notifications.css";
//import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="App">
      <Index />
    </div>
  );
}

export default App;
