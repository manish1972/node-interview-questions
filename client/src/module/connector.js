import { stringify } from "query-string";
const credintials_mode = "include";
const headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
};

export function get(path: string, query: Object = {}): Promise<any> {
  let url = path;
  return fetch(`${path}?${stringify(query)}`, {
    method: "GET",
    headers: headers,
    credentials: credintials_mode,
  }).then((result) => {
    return result.json();
  });
}

export function post(path: string, data: Object | FormData = {}): Promise<any> {
  let url = path;
  return fetch(url, {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data),
    credentials: credintials_mode,
  }).then((result) => {
    return result.json();
  });
}

export function del(path: string, data: Object | FormData = {}): Promise<any> {
  let url = path;
  return fetch(`${path}`, {
    method: "DELETE",
    headers: headers,
    credentials: credintials_mode,
  }).then((result) => {
    return result.json();
  });
}

export function put(path: string, data: Object | FormData = {}): Promise<any> {
  let url = path;
  return fetch(`${path}`, {
    method: "PUT",
    headers: headers,
    credentials: credintials_mode,
    body: JSON.stringify(data),
  }).then((result) => {
    return result.json();
  });
}
