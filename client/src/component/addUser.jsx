import React, { Component } from "react";
import * as Connector from "../module/connector";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment,
} from "semantic-ui-react";

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      age: "",
      displayErrorMessage: false,
      email: "",
      errorMessage: "",
      fullName: "",
      gender: "",
      location: "",
      loading: false,
      selectedUserId: 0,
      transactionType: "Add User",
      userName: "",
      userInfo: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      age: nextProps.userInfo.age,
      email: nextProps.userInfo.email,
      fullName: nextProps.userInfo.fullname,
      gender: nextProps.userInfo.gender,
      location: nextProps.userInfo.location,
      transactionType: nextProps.transactionType,
      selectedUserId: nextProps.selectedUserId,
      userName: nextProps.userInfo.username,
    });
    if (nextProps.clearControls === true) this.clearControls();
  }

  addUser = () => {
    const { selectedUserId, transactionType } = this.state;
    if (this.validateValues()) {
      this.setState({ loading: true });
      const { age, email, fullName, gender, location, userName } = this.state;

      var newUser = {
        age: age,
        email: email,
        fullname: fullName,
        gender: gender,
        location: location,
        username: userName,
      };
      if (transactionType === "Add User") {
        Connector.post("/users/adduser", newUser)
          .then((response) => {
            this.clearControls();
            this.props.transactionCompleted(
              response.success,
              response.msg,
              "addUser"
            );
          })
          .catch((err) => {
            this.props.transactionCompleted(
              false,
              "Some error occured while adding records "
            );
            this.setState({ loading: false });
            return;
          });
      } else {
        Connector.put(`/users/updateuser/${selectedUserId}`, newUser)
          .then((response) => {
            if (response.success) {
              this.clearControls();
            } else {
              this.setState({ loading: false });
            }
            this.props.transactionCompleted(
              response.success,
              response.msg,
              "addUser"
            );
          })
          .catch((err) => {
            this.props.transactionCompleted(
              false,
              "Some error occured while updation records"
            );
            this.setState({ loading: false });
            return;
          });
      }
    }
  };

  clearControls = () => {
    this.setState({
      age: "",
      deleteLoading: false,
      displayErrorMessage: false,
      email: "",
      errorMessage: "",
      fullName: "",
      gender: "",
      location: "",
      userName: "",
      loading: false,
      selectedUserId: 0,
      showDeleteConfirm: false,
      transactionType: "Add User",
      userInfo: [],
    });
  };

  onChange = (e, ctl) => {
    if (!e) return;
    this.setState({ [ctl]: e, displayErrorMessage: false });
  };

  validateValues() {
    const {
      age,
      email,
      fullName,
      gender,
      location,
      msg,
      userName,
    } = this.state;
    let errorMessage = "";

    if (userName === "" || userName === undefined) {
      errorMessage = "User Name cannot be left blank";
    } else if (fullName === "" || fullName === undefined) {
      errorMessage = "Full Name cannot be left blank";
    } else if (email === "" || email === undefined) {
      errorMessage = "Email cannot be left blank";
    } else {
      return true;
    }

    this.setState({ errorMessage: errorMessage, displayErrorMessage: true });
  }

  render() {
    const {
      age,
      displayErrorMessage,
      email,
      errorMessage,
      fullName,
      gender,
      location,
      loading,
      transactionType,
      userName,
    } = this.state;
    return (
      <div>
        <Header
          as="h3"
          block
          style={{ backgroundColor: "#00b5ad", color: "white" }}
        >
          {transactionType}
        </Header>
        <Grid>
          <Grid.Column style={{ maxWidth: "100%" }}>
            <Form size="large">
              <Segment raised color="teal">
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    icon="user"
                    iconPosition="left"
                    onChange={(e) => {
                      this.onChange(e.target.value, "userName");
                    }}
                    placeholder="User Name"
                    type="text"
                    value={userName}
                  />
                  <Form.Input
                    fluid
                    icon="address book"
                    iconPosition="left"
                    onChange={(e) => {
                      this.onChange(e.target.value, "fullName");
                    }}
                    placeholder="Full Name"
                    type="text"
                    value={fullName}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    icon="mail"
                    iconPosition="left"
                    onChange={(e) => {
                      this.onChange(e.target.value, "email");
                    }}
                    placeholder="Email"
                    type="text"
                    value={email}
                  />
                  <Form.Input
                    fluid
                    icon="calendar alternate"
                    iconPosition="left"
                    onChange={(e) => {
                      this.onChange(e.target.value, "age");
                    }}
                    placeholder="Age"
                    type="number"
                    value={age}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    icon="map marker alternate"
                    iconPosition="left"
                    onChange={(e) => {
                      this.onChange(e.target.value, "location");
                    }}
                    placeholder="Location"
                    type="text"
                    value={location}
                  />
                  <Form.Input
                    fluid
                    icon="male"
                    iconPosition="left"
                    onChange={(e) => {
                      this.onChange(e.target.value, "gender");
                    }}
                    placeholder="Gender"
                    type="text"
                    value={gender}
                  />
                </Form.Group>
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
        <br />

        <Message
          attached="bottom"
          color="red"
          content={errorMessage}
          onDismiss={() => {
            this.setState({ displayErrorMessage: false });
          }}
          hidden={!displayErrorMessage}
        />

        <Button
          class="ui button"
          color="teal"
          disabled={loading}
          loading={loading}
          onClick={() => {
            this.addUser();
          }}
        >
          Save
        </Button>
        <Button
          class="ui button"
          color="teal"
          onClick={() => {
            this.clearControls();
          }}
        >
          Clear
        </Button>
      </div>
    );
  }
}

export default AddUser;
