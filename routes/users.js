var express = require("express");
var router = express.Router();

/*
 * GET userlist.
 */
router.get("/userlist", function (req, res) {
  var db = req.db;
  var collection = db.get("userlist");
  collection.find({}, {}, function (err, docs) {
    res.json(!err ? docs : { msg: "Error fetching user list", success: false });
  });
});

/*
 * POST to adduser.
 */
router.post("/adduser", function (req, res) {
  var db = req.db;
  var collection = db.get("userlist");

  collection.count({ username: req.body.username }, function (err, result) {
    if (result > 0) {
      res.json(
        !err
          ? { msg: "User already Exist", success: false }
          : { msg: "Error in adding user", success: false }
      );
      return true;
    }

    collection.insert(req.body, function (err, result) {
      res.json(
        !err
          ? { msg: "User added successfully", success: true }
          : { msg: "Error in adding user", success: false }
      );
    });
  });
});

/*
 * DELETE to deleteuser.
 */
router.delete("/deleteuser/:id", function (req, res) {
  var db = req.db;
  var collection = db.get("userlist");
  var userToDelete = req.params.id;
  collection.remove({ _id: userToDelete }, function (err) {
    res.json(
      !err
        ? { msg: "User deleted successfully", success: true }
        : { msg: "Error in deleting user", success: false }
    );
  });
});

/*
 * UPDATE to updateuser.
 */
router.put("/updateuser/:id", function (req, res) {
  var db = req.db;
  var collection = db.get("userlist");
  var userToUpdate = req.params.id;

  collection.count(
    {
      username: req.body.username,
      _id: { $ne: userToUpdate },
    },
    function (err, result) {
      if (result > 0) {
        res.json(
          !err
            ? { msg: "User already Exist", success: false }
            : { msg: "Error updating user", success: false }
        );
        return true;
      }

      collection.update({ _id: userToUpdate }, req.body, function (err) {
        res.json(
          !err
            ? { msg: "User updated sucessfully", success: true }
            : {
                msg: "Error updating user",
                success: false,
              }
        );
      });
    }
  );
});

module.exports = router;
